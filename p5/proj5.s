# Glen Beebe & Christopher Patton

.globl __start
.globl print
.globl printint
.globl printstr
.globl strloop
.globl endstrloop
.globl printhex
.globl hexi
.globl hexa
.globl printchar
.globl printpoll
.globl printpollloop
.globl gotoprintpoll
.globl printintr
.globl gotoprintintr
.globl printintrloop
.globl endprintintrloop

.kdata
bignum: .word 0x40000000

cntrstr: .asciiz "\nThe counter value is: "
madeit: .asciiz "\nMade it back successfully!"

rctrl: .word 0xffff0000
rdata: .word 0xffff0004
tctrl: .word 0xffff0008
tdata: .word 0xffff000c

save0: .word 0x00000000
save1: .word 0x00000000
save2: .word 0x00000000
save3: .word 0x00000000

m1: .asciiz "Exception "
e0: .asciiz "[Interrupt] "
e1: .asciiz "N/A"
e2: .asciiz "N/A"
e3: .asciiz "N/A"
e4: .asciiz "\nAddress error in inst/data fetch "
e5: .asciiz "\nAddress error in store "
e6: .asciiz "\nBad instruction address "
e7: .asciiz "\nBad data address "
e8: .asciiz "N/A"
e9: .asciiz "N/A"
e10: .asciiz "N/A"
e11: .asciiz "N/A"
e12: .asciiz "\nArithmetic overflow "
excep: .word e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10
       .word e11, e12

statinit: .word 0x0000FF01
statreset: .word 0xFFFF00FC

.ktext 0x800000A0

__start:

# Addressing Error 1
la $a1, bignum
lw $a1, 0($a1)
lw $t9, 123($a1)

# Addressing Error 2
la $a1, bignum
lw $a1, 0($a1)
lw $t9, excep($a1)

# Arithmetic Error
la $a1, bignum
lw $t9, 0($a1)
add $t9, $t9, $t9

li $v0, 4
la $a0, madeit
li $a1, 1
jal print

# print counter
li $v0, 4
la $a0, cntrstr
li $a1, 0
jal print

li $v0, 1
add $a0, $s7, $0
li $a1, 0
jal print

#end prgm
li $v0, 10
syscall


print:
   la $s0, save0
   sw $ra, 0($s0)

   li $t0, 1
   beq $v0, $t0, printint
   li $t0, 4
   beq $v0, $t0, printstr
   li $t0, 0
   beq $v0, $t0, printhex

   la $s0, save0
   lw $ra, 0($s0)
   jr $ra

printint:
#   la $s0, save0
#   sw $ra, 0($s0)

   addi $a2, $a0, 48 # '0'
   la $s0, save0
   sw $ra, 0($s0)
   lw $ra, 0($s0)
   jal printchar

   la $s0, save0
   lw $ra, 0($s0)
   jr $ra

printstr:
#   la $s0, save0
#   sw $ra, 0($s0)

   strloop:
      and $a2, $0, $0
      lbu $a2, 0($a0)
      beq $a2, $0, endstrloop
      jal printchar
      addi $a0, $a0, 1
      j strloop

   endstrloop:
      la $s0, save0
      lw $ra, 0($s0)
      jr $ra

printhex:
#   la $s0, save0
#   sw $ra, 0($s0)

   addi $a2, $0, 48 # '0'
   jal printchar
   addi $a2, $0, 120 # 'x'
   jal printchar

   srl $a2, $a0, 28
   jal hextoascii

   srl $a2, $a0, 24
   andi $a2, $a2, 0x00FF
   jal hextoascii

   srl $a2, $a0, 20
   andi $a2, $a2, 0x00FF
   jal hextoascii

   srl $a2, $a0, 16
   andi $a2, $a2, 0x00FF
   jal hextoascii

   srl $a2, $a0, 12
   andi $a2, $a2, 0x00FF
   jal hextoascii

   srl $a2, $a0, 8
   andi $a2, $a2, 0x00FF
   jal hextoascii

   srl $a2, $a0, 4
   andi $a2, $a2, 0x00FF
   jal hextoascii

   andi $a2, $a2, 0x00FF
   jal hextoascii

   la $s0, save0
   lw $ra, 0($s0)
   jr $ra

hextoascii:
   la $s0, save1
   sw $ra, 0($s0)

   slti $t0, $a2, 10
   li $t1, 1
   beq $t0, $t1, hexi
   beq $t0, $0, hexa

   hexi:
      jal printint

      la $s0, save1
      lw $ra, 0($s0)
      jr $ra
   hexa:
      addi $a2, $a2, 65 # 'A'
      jal printchar

      la $s0, save1
      lw $ra, 0($s0)
      jr $ra

printchar:
   la $s0, save2
   sw $ra, 0($s0)

   beq $a1, $0, gotoprintpoll

   gotoprintintr:
      jal printpoll
      la $s0, save2
      lw $ra, 0($s0)
      jr $ra

   gotoprintpoll:
      jal printpoll
      la $s0, save2
      lw $ra, 0($s0)
      jr $ra

printpoll:
   la $s0, save3
   sw $ra, 0($s0)

   printpollloop:
      la $t0, tctrl
      lw $t0, 0($t0)
      lb $t1, 0($t0)
      andi $t1, $t1, 1
      beq $t1, $0, printpollloop

   la $t0, tdata
   lw $t0, 0($t0)
   sw $a2, 0($t0)

   la $s0, save3
   lw $ra, 0($s0)
   jr $ra

printintr:
   la $s0, save3
   sw $ra, 0($s0)

   mfc0 $s6, $12 ## get the status register
   la $t0, statinit
   lw $t1, 0($t0)
   or $s6, $s6, $t1
   andi $s6, $s6, 0xFFFD
   mtc0 $s6, $12

   la $t0, tctrl
   lw $t0, 0($t0)
   lw $t1, 0($t0)
   ori $t1, $t1, 2
   sw $t1 0($t0)

   printintrloop:
      addi $s7, $s7, 1
      j printintrloop

   endprintintrloop:
      mfc0 $s6, $12
      la $t0, statreset
      lw $t1, 0($t0)
      and $s6, $s6, $t1
      mtc0 $s6, $12

      la $t0, tctrl
      lw $t0, 0($t0)
      lw $t1, 0($t0)
      andi $t1, $t1, 0xFFFD
      sw $t1, 0($t0)

      la $s0, save3
      lw $ra, 0($s0)
      jr $ra


# Exception Handler
.ktext 0x80000180
exception_handler:
#extract exception code field and print according message
mfc0 $k0, $13 ## get cause register
srl $t1, $k0, 2
andi $t2, $k0, 0x3c

beq $t2, $0, INTR

mfc0 $k1, $14 ## get EPC

li $v0, 4
lw $a0, excep($t2)
li $a1, 1
jal print

#print epc
li $v0, 0
ori $a0, $k1, 0
li $a1, 1
jal print

li $t3, 12
beq $t1, $t3, ARITH

# add 4 to epc to skip offending instruction
addiu $k1, $k1, 4
mtc0 $k1, $14
eret    #return from error

INTR:
la $t0, tdata
lw $t0, 0($t0)
sw $a2, 0($t0)

#la $t1, endprintintrloop
#mtc0 $t1, $14
j endprintintrloop
eret

ARITH:
lui $t9, 0x400
eret
