/* fullAdd.h  
   
   CPE 315
   Full adder signature
   Written:  9/17/01

   Note: The const are important.
*/ 
                  
#include <Sim.h>

//  Adds A + B + Cin to produce Sum and Cout
void fullAdder(SD(sd), 

               // Inputs:
               const Signal& A,
               const Signal& B,
               const Signal& Cin,

               // Outputs:
               const Signal& Sum,
               const Signal& Cout
);
