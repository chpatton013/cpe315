/* fetch.c  
   Implementation of fetch.h

   CPE 315 - Professor Grimes
   Spring 2012

   Modifications: none.
*/ 
                  
#include <Sim.h>

// This requires that instructionMemory be declared and initialized in a file
// that uses this module.
extern unsigned char instructionMemory[];

// Define ROM geometry - 1024 words; each word is 32 bits
#define ROMSIZE 1024
#define WORDSIZE 32

void fetch(SD(sd), 
           // Inputs:
           const Signal& startRun,
           const Signal& enable,  
           const Signal& clock,

           // Outputs:
           const Signal& nextPC, 
           const Signal& PC,
           const Signal& instruction)
{
   Module((sd, "Fetch"),
          // Inputs:
          (startRun,
           enable,
           clock),

          // Outputs:
          (nextPC, 
           PC,
           instruction));
  
   // Bus from mux to PC
   Signal PCin(32);
  
   // Implementation of Fig. 4.6 plus circuit to initialize the PC.
   // Note: The Rom takes word addresses and requires that
   //       address.length == log2(ROMSIZE)
   Mux(SD(sd, "2b"), startRun, (nextPC, (28 * Zero, One, 3 * Zero)), PCin);
   Register(SD(sd, "2c"), enable, clock, PCin, PC);
   Adder(SD(sd, "1d"), (29 * Zero, One, 2 * Zero, PC), nextPC);
   Rom(SD(sd, "2d"), PC[11] - PC[2], instruction, ROMSIZE, WORDSIZE, instructionMemory);
}

