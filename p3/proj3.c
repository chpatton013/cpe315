/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include <Sim.h>
#include <loadRom.h>

#include "clockUnit.h"
#include "fetch.h"
#include "alu.h"
#include "signExt.h"

#define NUM_INST 16
#define BUS_WIDTH 32

unsigned char instructionMemory[4096];

void simnet() {
   Sig(clock, 1);
   Sig(startRun, 1);
   Signal PC(BUS_WIDTH, "PC");
   Sig(NextPC, BUS_WIDTH);
   Signal Instr(BUS_WIDTH, "Instruction");
   Sig(invClock, 1);

   Signal regDst;
   Signal regEnable;
   Signal regReadAddr(5);
   Signal regWriteAddr(5);
   Signal regReadData1(32);
   Signal regReadData2(32);
   Signal regReadData3(32, "Reg3");

   Signal aluSrc;
   Signal Cin;
   Signal Ainvert;
   Signal Binvert;
   Signal Op(2);
   Signal immed(32);
   Signal aluMuxOut(32);
   Signal aluRes(32);
   Signal C;
   Signal V;


   loadRom(instructionMemory, NUM_INST * 4, "proj3TestRom.txt");

   clockUnit("1a", startRun, clock);

   fetch("1a",
      startRun, One, clock,
      NextPC, PC, Instr
   );

   Switch("1a", regDst, 'd');
   Switch("1a", regEnable, 'w');

   Switch("1a", aluSrc, 'a');

   Switch("1a", Cin, 'C');
   Switch("1a", Ainvert, 'A');
   Switch("1a", Binvert, 'B');
   Switch("1a", Op[1], 'o');
   Switch("1a", Op[0], 'p');

   Switch("1a", regReadAddr[4], '4');
   Switch("1a", regReadAddr[3], '3');
   Switch("1a", regReadAddr[2], '2');
   Switch("1a", regReadAddr[1], '1');
   Switch("1a", regReadAddr[0], '0');

   int parts[] = {4};

   Probe("1g", PC, 1, parts);
   Probe("1h", Instr, 1, parts);

   Not("1b", clock, invClock);

   Mux("1b", regDst, (Instr[15] - Instr[11], Instr[20] - Instr[16]), regWriteAddr);

   RegisterFile("1d",
    (regEnable, invClock, regWriteAddr,
    regReadAddr, Instr[20] - Instr[16], Instr[25] - Instr[21]),
    aluRes,
    (regReadData3, regReadData2, regReadData1),
    32, 32, 5, 3, 1);

   signExt("1b", (Instr[15] - Instr[0]), immed);

   Mux("1d", aluSrc, (immed, regReadData2), aluMuxOut);

   alu("1e", regReadData1, aluMuxOut, Cin, Ainvert, Binvert, Op, aluRes, C, V);

   Probe("1f", regReadData3, 1, parts);
}
