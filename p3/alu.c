/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "alu.h"

void alu(SD(sd),
   const Signals& A,
   const Signals& B,
   const Signal& Cin,
   const Signal& Ainvert,
   const Signal& Binvert,
   const Signals& Op,

   const Signals& Res,
   const Signal& C,
   const Signal& V)
{
   Module((sd, "32 Bit ALU"),
      (A,B,Cin,Ainvert,Binvert,Op),
      (Res,C,V)
   );

   Signal Cout(NUM_BITS);
   Signal lessIn;
   Signal lessJunk(NUM_BITS-1);

   Signal Prod1;
   Signal Prod2;

   oneBitALU(SD(sd, "1d"), A[0], B[0], Cin, lessIn, Ainvert, Binvert, Op,
    Res[0], Cout[0], lessJunk[0]);

   for (int i = 1; i < NUM_BITS-1; ++i)
      oneBitALU(SD(sd,"1d"), A[i], B[i], Cout[i-1], Zero, Ainvert, Binvert, Op,
       Res[i], Cout[i], lessJunk[i]);

   oneBitALU(SD(sd,"1d"), A[NUM_BITS-1], B[NUM_BITS-1], Cout[NUM_BITS-2], Zero,
    Ainvert, Binvert, Op, Res[NUM_BITS-1], C, lessIn);

   Iand(SD(sd, "1d"), (Res[NUM_BITS-1]), (A[NUM_BITS-1],B[NUM_BITS-1]), Prod1);
   Iand(SD(sd, "1d"), (A[NUM_BITS-1], B[NUM_BITS-1]), (Res[NUM_BITS-1]), Prod2);
   Or(SD(sd, "1d"), (Prod1, Prod2), V);
}
