# 	Lab 3 Test Program
#
#  CPE 315
#
#  By: Dan Stearns
#  Description:  Use this program to test your circuit
#
#  Modifications:
#     10/24/10: Removed distracting formatting (Lehmer)

# Data Area
.data

# Text Area
.text

main:

# MIPS INSTRUCTIONS - CONTROL BITS & EXPECTED RESULT

addi $0, $0, 0
# $0 <- 0
# a, w, o, c
# PC = 0x8  IR = 20000000  Result = 0

addi $3, $0, 3
# $3 <- 3
# a, w, o, c
# PC = 0xC  IR = 20030003  Result = 3

addi $31, $3, -4
# $31 <- -1
# a, w, o, c
# PC = 0x10 IR = 207FFFFC  Result = FFFFFFFF

add $21, $31, $3
# $21 <- 2
# w, d, o, c
# PC = 0x14 IR = 03E3A820  Result = 00000002

slt $2, $21, $31
# $2 <- 0
# w, d, j, o, p, c
# PC = 0x18 IR = 02BF102A  Result = 00000000

slt $2, $31, $21
# $2 <- 1
# w, d, j, o, p, c
# PC = 0x1C IR = 03F5102A  Result = 00000001

addi $0, $0, 0
# $0 <- 0
# a, w, o, c
# PC = 0x20 IR = 20000000  Result = 00000000
