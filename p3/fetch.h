/* fetch.h  

   CPE 315 - Professor Grimes
   Spring 2012

   Written by:  Dan Stearns 

   This circuit is modeled on Fig. 4.6 in Patterson and Hennessy.
   In addition, the circuit includes a method to initialize the PC
   to a specified value.
*/

void fetch(SD(sd), 
           // Inputs:
           const Signal& startRun,     // Toggles initialization circuit
           const Signal& enable,       // Toggles fetch
           const Signal& clock,

           // Outputs:
           const Signals& nextPC,      // The new PC after a fetch
           const Signals& PC,          // Program Counter
           const Signals& instruction  // The fetched instruction
);

