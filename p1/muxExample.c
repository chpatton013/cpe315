/* muxTest.c 

   CPE 315
   Mux Test - a 4 input, 2 select line multiplexor

   Written by:  Dan Stearns

   Modifications:
     1/15/08 - dumped the Sig macros 
*/

#include <Sim.h>

void simnet() {
   Signal muxIn(4);
   Signal muxOut(1, "muxOut");
   Signal select(2);

   Space(SD("1b", "Note input/select line order: Incorrect order is a common mistake!"));       

   // Input lines
   Switch("1a", muxIn[3], '3');
   Space(SD("1a", "input[3]"));
   Switch("1a", muxIn[2], '2');
   Space(SD("1a", "input[2]"));
   Switch("1a", muxIn[1], '1');
   Space(SD("1a", "input[1]"));
   Switch("1a", muxIn[0], '0');
   Space(SD("1a", "input[0]"));

   // Select lines
   Switch("2b.1a", select[1], 'a');
   Space(SD("2b.2a", "select[1]"));
   Switch("2b.1b", select[0], 'b');
   Space(SD("2b.2b", "select[0]"));

   // select and muxIn are specified in MSB down to LSB order
   Mux("1b", select, muxIn, muxOut);

   Probe("1c", muxOut);
}
