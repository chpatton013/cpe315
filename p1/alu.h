#include <Sim.h>
#include "oneBitALU.h"

void alu(SD(sd), 

   // Inputs:
   const Signals& A,
   const Signals& B,
   const Signal& Cin,
   const Signal& Ainvert,
   const Signal& Binvert,
   const Signals& Op,

   // Outputs:
   const Signals& Sum,
   const Signal&  Carry,
   const Signal&  Overflow
);
