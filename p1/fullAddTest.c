/* fullAddTest.c  

   CPE 315
   Driver for 1-bit full adder
*/

#include <Sim.h>
#include "fullAdd.h"

void simnet() {

   Signal A;
   Signal B;
   Signal Cin;

   Signal Cout(1, "Cout");
   Signal Sum(1, "Sum");

   Switch("1a", A, 'a');
   Switch("1a", B, 'b');
   Switch("1a", Cin, 'c');

   fullAdder("1b", A, B, Cin, Sum, Cout);

   Probe("1c.1a", Sum);
   Space(SD("1c.1b", "Sum"));

   Probe("1c.2a", Cout);
   Space(SD("1c.2b", "Carry out"));
}
