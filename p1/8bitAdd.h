/* 8bitAdder.h  9-17-01 

   CPE 315
   Add8 signature

   Modifications:
     9/5/02 - added const

   Notes:
     1.  The const are important; don't omit them 
*/ 
                  
#include <Sim.h>

// Adds bits A and B and determines their Sum, Carry, and Overflow.
void Add8(SD(sd), 

          // Inputs:
          // The type Signals is used instead of Signal to indicate that a bus
          // (instead of a single Signal) is expected as an argument.
          // The Signals type should only be used in parameter lists.
          const Signals& A,
          const Signals& B,

          // Outputs:
          const Signals& Sum,
          const Signal&  Carry,
          const Signal&  Overflow
);
