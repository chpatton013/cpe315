#include <Sim.h>
#include "fullAdd.h"

void oneBitALU(SD(sd),
   // Inputs:
   const Signal& A,
   const Signal& B,
   const Signal& Cin,
   const Signal& Less,
   const Signal& Ainvert,
   const Signal& Binvert,
   const Signals& Op,

   // Outputs:
   const Signal& Res,
   const Signal& Cout
);
