#include <Sim.h>
#include "alu.h"

void simnet() {

   // Input and output signals and buses
   Signal A(8);
   Signal B(8);
   Signal Cin;
   Signal Ainvert;
   Signal Binvert;
   Signal Op(2);

   Signal Res(8, "Res");
   Signal Carry(1, "C");
   Signal Overflow(1, "V");

   // A bus input switches
   Switch("1a", A[7], '7');
   Switch("1a", A[6], '6');
   Switch("1a", A[5], '5');
   Switch("1a", A[4], '4');
   Switch("1a", A[3], '3');
   Switch("1a", A[2], '2');
   Switch("1a", A[1], '1');
   Switch("1a", A[0], '0');
   Space(SD("1a", "A Bus"));

   // B bus input switches
   Switch("2a", B[7], '&');
   Switch("2a", B[6], '^');
   Switch("2a", B[5], '%');
   Switch("2a", B[4], '$');
   Switch("2a", B[3], '#');
   Switch("2a", B[2], '@');
   Switch("2a", B[1], '!');
   Switch("2a", B[0], ')');
   Space(SD("2a", "B Bus"));

   // Op Switches
   Switch("3a", Cin, 'c');
   Switch("3a", Ainvert, 'i');
   Switch("3a", Binvert, 'j');
   Switch("3a", Op[1], 'o');
   Switch("3a", Op[0], 'p');

   alu("2b",
        //inputs:
        A, B, Cin, Ainvert, Binvert, Op,
        //outputs:
        Res,
        Carry,
        Overflow
   );

   int parts[] = {4};
   Probe("1c-2c.1a", Res, 1, parts );
   Space(SD("3c", "Res"));

   Space(SD("3b"));
   Probe("3b", Overflow);
   Space(SD("3b", "Overflow"));

   Probe("3c", Carry);
   Space(SD("3c", "Carry"));
}

