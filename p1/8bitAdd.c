/* 8bitAdd.c  

   CPE 315
   8-Bit Adder Module
   Written:  9/16/01 by Dan Stearns

   Modifications:
     3/27/02  -  used loop to construct adder
     9/05/02  -  added const

   Notes:  
     1.  The const are important; don't omit them
*/ 
                  
#include <Sim.h>
#include "fullAdd.h"

void Add8(SD(sd), 

          // Inputs:
          const Signals& A,
          const Signals& B,

          // Outputs:
          const Signals& Sum,
          const Signal&  C,
          const Signal&  V)
{
   Module((sd, "8 Bit Adder"),
          // Inputs:
          (A,B),
          // Outputs:
          (Sum,C,V)
   );
  
   Signal Cout(8);

   // Intermediate products used for V calculation
   Signal Prod1;
   Signal Prod2;

   // Construct the adder for bit 0
   fullAdder (SD(sd, "1d"), A[0], B[0], Zero, Sum[0], Cout[0]);

   // Construct the adders for bits 1-6 
   for (int i = 1; i <= 6; i++) 
      fullAdder (SD(sd,"1d"), A[i], B[i], Cout[i-1], Sum[i], Cout[i]);

   // Construct the adder for bit 7
   fullAdder (SD(sd,"1d"), A[7], B[7], Cout[6], Sum[7], C);
  
   // Compute V using sum of products
   Iand(SD(sd, "1d"), (Sum[7]), (A[7],B[7]), Prod1);
   Iand(SD(sd, "1d"), (A[7], B[7]), (Sum[7]), Prod2);
   Or(SD(sd, "1d"), (Prod1, Prod2), V);
}
