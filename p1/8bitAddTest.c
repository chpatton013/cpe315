/* 8bitAddTest.c  

   CPE 315
   Driver for 8 bit adder

   Modifications:
     9/23/02 - added Space for Probe text
     8/12/04 - made cosmetic improvements
*/

#include <Sim.h>
#include "8bitAdd.h"
#include "fullAdd.h"

void simnet() {

   // Input and output signals and buses
   Signal A(8);
   Signal B(8);
   Signal Sum(8, "Sum");
   Signal Carry(1, "C");
   Signal Overflow(1, "V");

   // A bus input switches
   Switch("1a", A[7], '7');
   Switch("1a", A[6], '6');
   Switch("1a", A[5], '5');
   Switch("1a", A[4], '4');
   Switch("1a", A[3], '3');
   Switch("1a", A[2], '2');
   Switch("1a", A[1], '1');
   Switch("1a", A[0], '0');
   Space(SD("1a", "A Bus"));

   // B bus input switches
   Switch("3a", B[7], '&');
   Switch("3a", B[6], '^');
   Switch("3a", B[5], '%');
   Switch("3a", B[4], '$');
   Switch("3a", B[3], '#');
   Switch("3a", B[2], '@');
   Switch("3a", B[1], '!');
   Switch("3a", B[0], ')');
   Space(SD("3a", "B Bus"));

   Add8("2b",
        //inputs:
        A, B,
        //outputs:
        Sum,
        Carry,
        Overflow
   );

   int parts[] = {4};
   Probe("1c-2c.1a", Sum, 1, parts );
   Space(SD("3c", "Sum"));

   Space(SD("3b"));
   Probe("3b", Overflow);
   Space(SD("3b", "Overflow"));

   Probe("3c", Carry);
   Space(SD("3c", "Carry"));
}

