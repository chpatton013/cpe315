/* oneBitALUTest.c

   CPE 315
   Driver for 1-bit full adder
*/

#include <Sim.h>
#include "oneBitALU.h"

void simnet() {

   Signal A;
   Signal B;
   Signal Cin;
   Signal Less;
   Signal Ainvert;
   Signal Binvert;
   Signals Op(2);

   Signal Cout(1, "Cout");
   Signal Res(1, "Res");

   Switch("1a.1a", A, 'a');
   Switch("1a.1a", B, 'b');
   Switch("1a.2a", Cin, 'c');
   Switch("1a.2a", Less, 'l');

   Switch("1a.3a", Ainvert, 'A');
   Switch("1a.3a", Binvert, 'B');

   Switch("1a.4a", Op[1], 'O');
   Switch("1a.4a", Op[0], 'o');

   oneBitALU("1b", A, B, Cin, Less, Ainvert, Binvert, Op, Res, Cout);

   Probe("1c.1a", Res);
   Space(SD("1c.1b", "Res"));

   Probe("1c.2a", Cout);
   Space(SD("1c.2b", "Carry out"));
}
