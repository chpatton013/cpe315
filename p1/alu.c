#include <Sim.h>
#include "alu.h"

void alu(SD(sd),

   // Inputs:
   const Signals& A,
   const Signals& B,
   const Signal& Cin,
   const Signal& Ainvert,
   const Signal& Binvert,
   const Signals& Op,

   // Outputs:
   const Signals& Res,
   const Signal&  C,
   const Signal&  V)
{
   Module((sd, "8 Bit ALU"),
          // Inputs:
          (A,B,Cin,Ainvert,Binvert,Op),
          // Outputs:
          (Res,C,V)
   );

   Signal Cout(8);

   // Intermediate products used for V calculation
   Signal Prod1;
   Signal Prod2;

   // Construct Adders
   oneBitALU(SD(sd, "1d"), A[0], B[0], Cin, Zero, Ainvert, Binvert, Op, Res[0], Cout[0]);

   for (int i = 1; i < 7; i++)
      oneBitALU(SD(sd,"1d"), A[i], B[i], Cout[i-1], Zero, Ainvert, Binvert, Op, Res[i], Cout[i]);

   oneBitALU(SD(sd,"1d"), A[7], B[7], Cout[6], Zero, Ainvert, Binvert, Op, Res[7], C);

   // Compute V using sum of products
   Iand(SD(sd, "1d"), (Res[7]), (A[7],B[7]), Prod1);
   Iand(SD(sd, "1d"), (A[7], B[7]), (Res[7]), Prod2);
   Or(SD(sd, "1d"), (Prod1, Prod2), V);
}
