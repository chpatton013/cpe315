/* clockUnit.h  

   Written by:  Dan Stearns
   CPE 315

   clockUnit is a manual clock used for the CPE 315 SIM labs.

   To operate this manual clock:

   r  key toggles startRun signal;  it's used to initialize or reset
      other circuits.  It's put into the clockUnit as a convenience
      for designers.

   c  key generates one clock pulse of duration 1000.  The number has no
      units; it's an artificial measure (simulation time) which varies from
      machine to machine.
*/

void clockUnit(SD sd, const Signal& startRun, const Signal& clock);
