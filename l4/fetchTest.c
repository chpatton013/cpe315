/* fetchTest.c  
   Driver for lab exercise #4  fetch circuit.

   CPE 315 - Professor Grimes
   Spring 2012

   Written by:  Dan Stearns

   Modifications: none.

   Note:  to operate this circuit press the keys as shown
   
      c    to initialize PC
      r    to turn off initialization
      c    to generate each successive clock
*/

#include <loadRom.h>
#include <Sim.h>

#include "clockUnit.h"
#include "fetch.h"

// Number of instructions in romfile
#define NUM_INST 16

// Stores instructions in 4K memory; see Fig. 4.5 on page 308.
unsigned char instructionMemory[4096];

// Width of buses in fetch circuit
#define BUS_WIDTH 32

void simnet() {
   Sig(clock, 1);
   Sig(startRun, 1);
   Signal PC(BUS_WIDTH, "PC");
   Sig(NextPC, BUS_WIDTH);
   Signal Instruction(BUS_WIDTH, "Instruction");

   // Load program into instruction memory
   loadRom (instructionMemory, NUM_INST * 4, "romfile.txt");

   clockUnit("2a", startRun, clock);

   fetch("2b",
         // Inputs:
         startRun,
         One, 
         clock,

         // Outputs:
         NextPC,
         PC,
         Instruction
   );

   int parts[] = {4};

   Probe("1c-3c", PC, 1, parts);
   Probe("1d-3d", Instruction, 1, parts);
}

