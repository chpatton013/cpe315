#include <Sim.h>
#include "oneBitALU.h"

#define NUM_BITS 32

void alu(SD(sd),
   const Signals& A,
   const Signals& B,
   const Signal& Cin,
   const Signal& Ainvert,
   const Signal& Binvert,
   const Signal& Unsgn,
   const Signals& Op,

   const Signals& Sum,
   const Signal& C,
   const Signal& V
);
