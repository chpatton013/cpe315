#include <loadRom.h>
#include <Sim.h>

#define ROMSIZE 16
#define WORDSIZE 8

#define NUM_WORDS 12

unsigned char romContents[NUM_WORDS];

/*
 * switches between:
 * ADD
 * ADDU
 * AND
 * DIV
 * DIVU
 * MULT
 * MULTU
 * OR
 * SLT
 * SLTU
 * SUB
 * SUBU
 *
 * 12 possibilities = 4 bits needed for in
 *
 * spits out:
 * Cin
 * Ainvert
 * Binvert
 * Unsgn
 * Op(2)
 *
 * 6 output signals
 */

void simnet() {
   Signal address(4, "address");
   Switch("1a", address[3], '3');
   Switch("1a", address[2], '2');
   Switch("1a", address[1], '1');
   Switch("1a", address[0], '0');

   Signal bits(8, "ROM Output");

   loadRom(romContents, NUM_WORDS, "romfile.txt");

   Rom("1b", address, bits, ROMSIZE, WORDSIZE, romContents);

   Probe("1c", bits);
}

