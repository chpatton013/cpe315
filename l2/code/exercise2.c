/* exercise2.c

   CPE 315 - Professor Grimes
   Laboratory Exercise #2: Gates Demonstration
   Written by: Michigan State Students, 1993
   Comments by: Andrew Lehmer
*/

// This line must be at the top of every file for a SIM simulation.
#include "Sim.h"

// SIM simulations do not need a main() function. Instead, simnet() is used.
void simnet() {
   // This is how to declare signals that won't have probes.
   // That means either the signal will be used for input, or it is an
   // intermediate signal.
   Signal a;
   Signal b;
   Signal c;

   // A bus is multiple signals with a single name or purpose.
   // This is how to declare a bus that is 3 bits wide.
   Signal out(3);

   // This is how to declare a signal that will be probed.
   // This signal is 1 bit wide, and its name will appear as "f" in the
   // simulation window.
   Signal f(1, "f");

   // Switches are used for user inputs. When a switch's corresponding key is
   // pressed, it toggles between ON and OFF (or HIGH and LOW).
   // The first switch below will be positioned at "1a" (explained below),
   // affects previously declared Signal a, and is toggled by the 'a' key on
   // the keyboard.
   Switch("1a", a, 'a');
   Switch("1a", b, 'b');
   Switch("1a", c, 'c');

   // The first argument in those switch constructions specify a position in
   // an expandable grid within the simulation. "1a" is the top left corner of
   // the grid. "2a" would be directly below "1a", and "1b" would be directly
   // to the right of "1a". Multiple elements in a given grid location will be
   // stacked vertically, so, in the case of the three Switches above, a will
   // be above b which will be above c.

   // Now that our inputs (a, b, and c) and outputs (f) have been defined, we
   // will define the inner workings of our circuit using common logic gates.
   // This circuit doesn't actually do anything useful, but it is an example of
   // how to place gates and connect them together.

   // This is a NOR gate.
   // Like the Switch, the first argument must be a position. This will appear
   // to the right of the Switches.
   // The second argument is (a, c). It is important that a and c be in
   // parenthesis because all input signals must be contained in a single
   // argument.
   // Finally, a SINGLE Signal, out[2] (explained below), is listed as the
   // third argument, which is the output.
   Nor("1b", (a, c), out[2]);

   // You can think of a bus, such as out, as an array of Signals. When a bus
   // is declared, it must be given a width. In the case of out, it has a width
   // of 3, so the last signal (always the MSB in SIM) is at index 2.
   // Therefore, the NOR gate output is connected to line 2 of out (out[2]).

   // The following is an IAND gate. Some of an IAND's inputs are inverted
   // (hence, the "I"), and some of its inputs are not inverted. Beyond that,
   // it behaves exactly like an AND gate.
   // The first argument should look familiar to you by now. :)
   // The second argument lists the NON-inverted inputs (in this case, there is
   // only one signal, b).
   // The third argument lists the inverted inputs.
   // The fourth, and final, argument is the output signal.
   Iand("1b", b, a, out[1]);

   // This is an AND gate. Its syntax is similar to the NOR seen earlier.
   And("1b", (a, c), out[0]);

   // You should have noticed by now that all three of the gates (NOR, IAND,
   // and AND) will be placed in the same grid position ordered vertically.

   // This is an OR gate.
   // Since out is a 3-signal bus, that implicitly makes this a 3-input OR.
   Or("1c", out, f);

   // In order to see what the value of f is, we need to Probe it (just like in
   // a real circuit!).
   // Probes need a position and a Signal.
   Probe("1d", f);

   // The Space give you a way to add labels to the simulation schematic.
   // It takes one argument, an SD.
   // SD stands for "schematic descriptor."
   // SD's only have a position and a name. Use your intuition to predict what
   // the following line will do.
   Space(SD("1e", "Output"));
}
