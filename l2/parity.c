#include "Sim.h"

void simnet() {
   Signal a;
   Signal b;
   Signal c;
   Signal d;

   Signal out(2);

   Signal f(1, "f");

   Switch("1a", a, 'a');
   Switch("1a", b, 'b');
   Switch("1a", c, 'c');
   Switch("1a", d, 'd');

   Xnor("1b", (a, b), out[1]);
   Xnor("1b", (c, d), out[0]);

   Xnor("1c", out, f);

   Probe("1d", f);

   Space(SD("1e", "Output"));
}
