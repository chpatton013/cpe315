/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "control.h"

void control(SD(sd),
   const Signals& OpCode,
   const Signals& Out)
{
   Signal bits(CNTRL_WORDSIZE, "ROM Output");

   loadRom(controlRom, CNTRL_ROMSIZE * CNTRL_WORDSIZE, "controlUnitRom.txt");
   Rom(SD(sd, "1a"), OpCode, bits, CNTRL_ROMSIZE, CNTRL_WORDSIZE, controlRom);

   for (int ndx = 0; ndx < CNTRL_NUM_BITS; ++ndx)
      Or(SD(sd, "1a"), (bits[ndx], Zero), Out[ndx]);
}
