/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "mmu.h"

void mmu(SD(sd),
   const Signals& VPC,
   const Signals& PPC)
{
   Module((sd, "MMU"),
      VPC, PPC
   );

   Signal bits(MMU_WORDSIZE, "Pagetable");

   loadRom(mmuRom, (MMU_ROMSIZE * MMU_WORDSIZE) / 8, "pagetable.txt");
   Rom(SD(sd, "1a"), VPC[12] - VPC[OFFSET], bits, MMU_ROMSIZE, MMU_WORDSIZE, mmuRom);

   for (int ndx = 0; ndx < OFFSET; ++ndx)
      Or(SD(sd, "1a"), (VPC[ndx], Zero), PPC[ndx]);
   for (int ndx = 0; ndx < 3; ++ndx)
      Or(SD(sd, "1a"), (bits[ndx], Zero), PPC[ndx + OFFSET]);
}
