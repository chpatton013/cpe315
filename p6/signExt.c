/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "signExt.h"

void signExt(SD(sd), const Signals& in, const Signals& out) {
   int ndx;

   Module((sd, "signExt"), (in), (out));

   for (ndx = 0; ndx < IN_SIZE; ++ndx)
      And(SD(sd, "1a"), (in[ndx], One), out[ndx]);
   for (; ndx < OUT_SIZE; ++ndx)
      And(SD(sd, "1a"), (in[IN_SIZE-1], One), out[ndx]);
}

