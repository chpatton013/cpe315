/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef ALU_CONTROL_H
#define ALU_CONTROL_H

#include <Sim.h>
#include <loadRom.h>

#define ALUC_NUM_BITS 6
#define ALUC_ROMSIZE 16
#define ALUC_WORDSIZE 8

static unsigned char aluControlRom[ALUC_ROMSIZE * ALUC_WORDSIZE];

void aluControl(SD(sd),
   const Signals& OpCode,
   const Signals& Out
);

#endif
