/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "aluControl.h"

void aluControl(SD(sd),
   const Signals& Funct,
   const Signals& Out)
{
   Signal bits(ALUC_WORDSIZE, "ROM Output");
   Signal Add(ALUC_NUM_BITS);
   Signal Sub(ALUC_NUM_BITS);
   Signal Oper(ALUC_NUM_BITS);
   Signal LUI(ALUC_NUM_BITS);

   loadRom(aluControlRom, ALUC_ROMSIZE * ALUC_WORDSIZE, "aluControlUnitRom.txt");
   Rom(SD(sd, "1a"), (Funct[3] - Funct[0]), bits, ALUC_ROMSIZE, ALUC_WORDSIZE, aluControlRom);

   for (int ndx = 0; ndx < ALUC_NUM_BITS; ++ndx)
      Or(SD(sd, "1a"), (bits[ndx], Zero), Oper[ndx]);

   Or(SD(sd, "1a"), (Zero, Zero), LUI[5]);
   Or(SD(sd, "1a"), (Zero, Zero), LUI[4]);
   Or(SD(sd, "1a"), (Zero, Zero), LUI[3]);
   Or(SD(sd, "1a"), (One, Zero), LUI[2]);
   Or(SD(sd, "1a"), (Zero, Zero), LUI[1]);
   Or(SD(sd, "1a"), (Zero, Zero), LUI[0]);

   Or(SD(sd, "1a"), (One, Zero), Sub[5]);
   Or(SD(sd, "1a"), (One, Zero), Sub[4]);
   Or(SD(sd, "1a"), (Zero, Zero), Sub[3]);
   Or(SD(sd, "1a"), (Zero, Zero), Sub[2]);
   Or(SD(sd, "1a"), (One, Zero), Sub[1]);
   Or(SD(sd, "1a"), (Zero, Zero), Sub[0]);

   Or(SD(sd, "1a"), (Zero, Zero), Add[5]);
   Or(SD(sd, "1a"), (Zero, Zero), Add[4]);
   Or(SD(sd, "1a"), (Zero, Zero), Add[3]);
   Or(SD(sd, "1a"), (Zero, Zero), Add[2]);
   Or(SD(sd, "1a"), (One, Zero), Add[1]);
   Or(SD(sd, "1a"), (Zero, Zero), Add[0]);

   Mux(SD(sd, "1a"), (Funct[5] - Funct[4]), (LUI, Oper, Sub, Add), Out);
}
