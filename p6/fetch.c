/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include <Sim.h>
#include <loadRom.h>

#include "mmu.h"

#define FETCH_ROMSIZE 512
#define FETCH_WORDSIZE 32

unsigned char instructionMemory[(FETCH_ROMSIZE * FETCH_WORDSIZE) / 8];

void fetch(SD(sd),
   const Signal& startRun,
   const Signal& enable,
   const Signal& clock,
   const Signal& PCin,

   const Signal& nextPC,
   const Signal& PC,
   const Signal& instruction)
{
   Module((sd, "Fetch"),
       (startRun, enable, clock, PCin),
       (nextPC, PC, instruction)
   );

   Signal PCtemp(32, "PCtemp");
   Signal PPC(32, "PPC");

   loadRom(instructionMemory, (FETCH_ROMSIZE * FETCH_WORDSIZE) / 8, "lab6rom.txt");

   Mux(SD(sd, "2b"), startRun, (PCin, (28 * Zero, One, 3 * Zero)), PCtemp);
   Register(SD(sd, "2c"), enable, clock, PCtemp, PC);
   Adder(SD(sd, "1d"), (29 * Zero, One, 2 * Zero, PC), nextPC);
   mmu(SD(sd, "1a"), PC, PPC[10] - PPC[0]);
   Rom(SD(sd, "2d"), PPC[10] - PPC[2], instruction,
    FETCH_ROMSIZE, FETCH_WORDSIZE, instructionMemory);
}

