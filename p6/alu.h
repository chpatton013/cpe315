/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef ALU_H
#define ALU_H

#include <Sim.h>
#include "oneBitALU.h"

#define NUM_BITS 32

void alu(SD(sd),
   const Signals& A,
   const Signals& B,
   const Signal& Cin,
   const Signal& Binvert,
   const Signal& Unsgn,
   const Signal& LUI,
   const Signals& Op,

   const Signals& Sum,
   const Signal& C,
   const Signal& V
);

#endif
