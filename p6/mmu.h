/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef MMU_H
#define MMU_H

#include <Sim.h>
#include <loadRom.h>

#define MMU_ROMSIZE 32
#define MMU_WORDSIZE 8

#define OFFSET 8

static unsigned char mmuRom[(MMU_ROMSIZE * MMU_WORDSIZE) / 8];

void mmu(SD(sd),
   const Signals& VPC,
   const Signals& PPC
);

#endif
