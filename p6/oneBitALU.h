/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef ONE_BIT_ALU_H
#define ONE_BIT_ALU_H

#include <Sim.h>
#include "fullAdd.h"

void oneBitALU(SD(sd),
   const Signal& A,
   const Signal& B,
   const Signal& Cin,
   const Signal& Less,
   const Signal& Binvert,
   const Signals& Op,

   const Signal& Res,
   const Signal& Cout,
   const Signal& Sum
);

#endif
