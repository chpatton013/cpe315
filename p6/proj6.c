/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include <Sim.h>

#include "clockUnit.h"
#include "fetch.h"
#include "alu.h"
#include "signExt.h"
#include "control.h"
#include "aluControl.h"
#include "mmu.h"

#define BUS_WIDTH 32

void simnet() {
   int parts[] = {4};

   Signal Clock;
   Signal InvClock;
   Signal StartRun;
   Signal PC(BUS_WIDTH, "PC");
   Signal NextPC(BUS_WIDTH, "NextPC");
   Signal Instr(BUS_WIDTH, "Instr");

   Signal RegDst;
   Signal Jump;
   Signal Branch;
   Signal MemToReg;
   Signal ALUOp(2);
   Signal MemWrite;
   Signal ALUSrc;
   Signal RegWrite;

   Signal RegReadAddr(5);
   Signal RegWriteAddr(5);
   Signal RegReadData1(32);
   Signal RegReadData2(32);
   Signal RegReadData3(32, "DestReg");

   Signal ALUCtrl(6, "ALUCtrl");

   Signal Immed(32);
   Signal ALUMuxOut(32);
   Signal ALURes(32, "ALURes");
   Signal C;
   Signal V;

   Signal BranchTemp(3);
   Signal BranchALURes(32, "BranchALURes");
   Signal BranchC;
   Signal BranchV;
   Signal BranchMuxOut(32, "BranchMuxOut");
   Signal JumpMuxOut(32, "JumpMuxOut");

   Signal MMURAM(11, "MMURAM");
   Signal RAMReadData(32, "RamReadData");
   Signal ALURAMMux(32, "ALURAMMux");

   Probe("1a", PC, 1, parts);
   Probe("1b", Instr, 1, parts);

   Not("1c", Clock, InvClock);
   clockUnit("1c", StartRun, Clock);
   fetch("1c",
      StartRun, One, InvClock, JumpMuxOut,
      NextPC, PC, Instr
   );

   control("1d", (Instr[31] - Instr[26]),
    (RegDst, Jump, Branch, MemToReg, ALUOp, MemWrite, ALUSrc, RegWrite));
   Mux("1d", RegDst, (Instr[15] - Instr[11], Instr[20] - Instr[16]), RegWriteAddr);

   RegisterFile("1e", (RegWrite, InvClock, RegWriteAddr,
    RegWriteAddr, Instr[20] - Instr[16], Instr[25] - Instr[21]),
    ALURAMMux, (RegReadData3, RegReadData2, RegReadData1), 32, 32, 5, 3, 1);
   signExt("1e", (Instr[15] - Instr[0]), Immed);

   aluControl("1f", (ALUOp, Instr[3] - Instr[0]), ALUCtrl);
   Mux("1f", ALUSrc, (Immed, RegReadData2), ALUMuxOut);

   alu("1g", RegReadData1, ALUMuxOut,
    ALUCtrl[5], ALUCtrl[4], ALUCtrl[3], ALUCtrl[2], ALUCtrl[1] - ALUCtrl[0],
    ALURes, C, V);
   alu("1g", NextPC, (Immed[29] - Immed[0], Zero, Zero),
    Zero, Zero, Zero, Zero, (One, Zero),
    BranchALURes, BranchC, BranchV);

   mmu("1g", ALURes, MMURAM);
   Ram("1h", (MemWrite, Clock, 2 * (MMURAM[8] - MMURAM[0])),
    RegReadData2, RAMReadData, 512, 32, 9, 1, 1);

   And("1h", (BranchTemp[1], Branch), BranchTemp[2]);
   Not("1h", BranchTemp[0], BranchTemp[1]);
   Or("1h", ALURes, BranchTemp[0]);

   Mux("1i", MemToReg, (RAMReadData, ALURes), ALURAMMux);
   Mux("1i", BranchTemp[2], (BranchALURes, NextPC), BranchMuxOut);
   Mux("1i", Jump, ((NextPC[31] - NextPC[28], Instr[25] - Instr[0], 2 * Zero),
    BranchMuxOut), JumpMuxOut);

   Probe("1j", RegReadData3, 1, parts);
   Probe("1k", ALURes, 1, parts);
   Probe("1l", RAMReadData, 1, parts);
}
