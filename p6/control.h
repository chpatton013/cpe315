/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef CONTROL_H
#define CONTROL_H

#include <Sim.h>
#include <loadRom.h>

#define CNTRL_NUM_BITS 9
#define CNTRL_ROMSIZE 64
#define CNTRL_WORDSIZE 16

static unsigned char controlRom[CNTRL_ROMSIZE * CNTRL_WORDSIZE];

void control(SD(sd),
   const Signals& OpCode,
   const Signals& Out
);

#endif
