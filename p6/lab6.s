#   Lab #6 test file 
#   CPE 315
#   Winter 2008
#
# Written By:       Dan Stearns
#
# Modifications:
#
# Notes:
#        1.  This codes starts at PC == 8.  Don't change it for any reason.
#        2.  The page size == 256 bytes.
#        3.  Don't try to run this program; it's for documention purposes only
#  Data Area
.data

.text 8

main:

# page 0 code

# verify add instructions
addi    $2, $0, 0x1234         
addi    $3, $0, 0x5678        
j       page1


# page 1 and page 2  code
.text 0x1F8
page1:
add     $4, $2, $3            

#verify 16 bit sw instruction
sw      $4, 0x60($0)

#verify subu
sub     $5, $3, $2            
sw      $5, 0x64($0)

#verify and & or
and     $6, $2, $3            
sw      $6, 0x68($0)
or      $7, $2, $3           

#verify write to address outside physical memory
sw      $7, 0x148C($0)

#verify sltu, both ways
addi    $8, $0, 0x7fff
slt     $8, $2, $3             # do screen print of $8
addi    $9, $0, 0x7fff
slt     $9, $3, $2             # do screen print of $9
j       page14 

#page 14 code; be sure your RAM does not contain these addresses.
.text 0x1400
page14:
#verify lw instruction and 32 bit address computation
#verify read from address outside physical memory
addi    $10, $0, 0x20
lw      $11, 0x40($10)         # do screen print of $11
lw      $12, 0x44($10)         # do screen print of $12
lw      $13, 0x48($10)         # do screen print of $13
lw      $14, 0x146C($10)       # do screen print of $14
