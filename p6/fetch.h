/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

void fetch(SD(sd),
   const Signal& startRun,
   const Signal& enable,
   const Signal& clock,
   const Signals& PCin,

   const Signals& nextPC,
   const Signals& PC,
   const Signals& instruction
);

