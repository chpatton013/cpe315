/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "oneBitALU.h"

void oneBitALU(SD(sd),
   const Signal& A,
   const Signal& B,
   const Signal& Cin,
   const Signal& Less,
   const Signal& Binvert,
   const Signals& Op,

   const Signal& Res,
   const Signal& Cout,
   const Signal& Sum)
{
   Module((sd, "One Bit ALU"),
      (A, B, Cin, Less, Binvert, Op),
      (Res, Cout)
   );

   Signal NotA, NotB, MuxB, MAAndMB, MAOrMB;

   Not(SD(sd, "1a"), (A), NotA);
   Not(SD(sd, "1a"), (B), NotB);

   Mux(SD(sd, "1b"), (Binvert), (NotB, B), MuxB);

   And(SD(sd, "1c"), (A, MuxB), (MAAndMB));
   Or(SD(sd, "1c"), (A, MuxB), (MAOrMB));

   fullAdder(SD(sd, "1c"), A, MuxB, Cin, Sum, Cout);

   Mux(SD(sd, "1d"), Op, (Less, Sum, MAOrMB, MAAndMB), Res);
}
