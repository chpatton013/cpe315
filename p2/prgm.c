#include <stdio.h>
#include <string.h>

int funct(int ndx) {
   if (ndx < 0) return 0;
   else if (ndx) return 6 + funct(ndx-1);
   else return 4;
}

int input() {
   int ndx;

   printf("enter an integer: ");
   scanf("%d", &ndx);

   return ndx;
}

char *prompt() {
   char in;

   printf("continue? (y/n): ");
   while (!strchr("YyNn", (in = fgetc(stdin))))
      ;

   return strchr("Yy", in);
}

int main(void) {
   int ndx;

   do {
      ndx = input();
      printf("perimeter is %d\n", funct(ndx));
   } while (prompt());

   return 0;
}
