# Project 2
#



# functions
.globl main
.globl input
.globl prompt
.globl funct

#labels
.globl yes
.globl base
.globl rtn

# variables
.globl input_str
.globl prompt_str
.globl answer_str
.globl newline

# data area
.data

input_str:
.asciiz " enter an integer: "
prompt_str:
.asciiz " continue (y/n): "
answer_str:
.asciiz " perimter is "
newline:
.asciiz "\n"

# instruction area
.text

main:

jal input

addi $a1, $v0, 0
andi $v1, $v1, 0
jal funct

la $a0, answer_str
addi $v0, $0, 4
syscall

addi $a0, $v1, 0
addi $v0, $0, 1
syscall

la $a0, newline
addi $v0, $0, 4
syscall

jal prompt

la $a0, newline
addi $v0, $0, 4
syscall

j main


#
.ent input
input:

# print input_str
la $a0, input_str
addi $v0, $0, 4
syscall

# get integer
addi $v0, $0, 5
syscall

# end func
jr $ra


#
.ent prompt
prompt:

# print prompt_str
la $a0, prompt_str
addi $v0, $0, 4
syscall

# get response
addi $v0, $0, 12
syscall

# compare response to 'y'
addi $t0, $0, 0x79
beq $t0, $v0, yes

# if no, exit
addi $v0, $0, 10
syscall

# end func
yes:
jr $ra


#
.ent funct
funct:

#rtn addr
sw $ra, 0($sp)
addi $sp, $sp, -4

# determine if base case
beq $a1, $0, base

addi $a1, $a1, -1
addi $v1, $v1, 6

jal funct
j rtn

base:
addi $v1, $v1, 4

rtn:
addi $sp, $sp, 4
lw $ra, 0($sp)
jr $ra
