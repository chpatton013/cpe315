# xSpim Demo Program
#
#   CPE 315
#   Spring 2011
#
# By: Dan Stearns
# Description:
#        This program adds two numbers and displays the sum.
#
# Notes:
#        The constants in the address loading sequence (LUI/ORI)
#        are entered from the global symbol table because...
#        this assembler lacks assembly-time arithmetic operations

# Declare global so programmer can see actual addresses.
.globl welcome
.globl prompt
.globl sumText

# Data Area
.data

welcome:
.asciiz " This program adds two numbers \n\n"

prompt:
.asciiz " Enter an integer: "

sumText:
.asciiz " \n Sum = "

# Text Area (i.e. instructions)
.text

main:

# Display the welcome message. LUI and ORI constants are obtained
# manually from the global symbol table. One will surely appreciate
# the LA pseudo-instruction after this exercise.
ori     $v0, $0, 4
lui     $a0, 0x1001
ori     $a0, $a0,0
syscall

# Clear $t0 for the sum
ori     $t0, $0, 0

# Display prompt
ori     $v0, $0, 4
lui     $a0, 0x1001
ori     $a0, $a0,0x22
syscall

# Read first integer and add to sum
ori     $v0, $0, 5
syscall
addu    $t0, $v0, $t0

# Display prompt
ori     $v0, $0, 4
lui     $a0, 0x1001
ori     $a0, $a0,0x22
syscall

# Read second integer and add to sum
ori	$v0, $0, 5
syscall
addu    $t0, $v0, $t0

# Display the sum text
ori     $v0, $0, 4
lui     $a0, 0x1001
ori     $a0, $a0,0x36
syscall

# Display the sum
ori     $v0, $0, 1
add 	$a0, $t0, $0
syscall

# Exit
ori     $v0, $0, 10
syscall
