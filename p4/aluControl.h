/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef ALU_CONTROL_H
#define ALU_CONTROL_H

#include <Sim.h>
#include <loadRom.h>

#ifdef NUM_BITS
#undef NUM_BITS
#endif
#define NUM_BITS 6

#ifdef ROMSIZE
#undef ROMSIZE
#endif
#define ROMSIZE 16

#ifdef WORDSIZE
#undef WORDSIZE
#endif
#define WORDSIZE 8

static unsigned char aluControlRom[ROMSIZE * WORDSIZE];

void aluControl(SD(sd),
   const Signals& OpCode,
   const Signals& Out
);

#endif
