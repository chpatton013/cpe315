/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include <Sim.h>
#include <loadRom.h>

#define FETCH_ROMSIZE 1024
#define FETCH_WORDSIZE 32

unsigned char instructionMemory[(FETCH_ROMSIZE * FETCH_WORDSIZE) / 8];

void fetch(SD(sd),
   const Signal& startRun,
   const Signal& enable,
   const Signal& clock,
   const Signal& PCin,

   const Signal& nextPC,
   const Signal& PC,
   const Signal& instruction)
{
   Module((sd, "Fetch"),
       (startRun, enable, clock, PCin),
       (nextPC, PC, instruction)
   );

   Signal PCtemp(32, "PCtemp");

   loadRom(instructionMemory, (FETCH_ROMSIZE * FETCH_WORDSIZE) / 8, "proj4TestRom.txt");

   Mux(SD(sd, "2b"), startRun, (PCin, (28 * Zero, One, 3 * Zero)), PCtemp);
   Register(SD(sd, "2c"), enable, clock, PCtemp, PC);
   Adder(SD(sd, "1d"), (29 * Zero, One, 2 * Zero, PC), nextPC);
   Rom(SD(sd, "2d"), PC[11] - PC[2], instruction,
    FETCH_ROMSIZE, FETCH_WORDSIZE, instructionMemory);
}

