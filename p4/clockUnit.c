/* clockUnit.c

   Implementation of clockUnit.h
    
   Written by:  Dan Stearns
   CPE 315
*/

#include <Sim.h>
#include "clockUnit.h"

void clockUnit(SD sd, const Signal& startRun, const Signal& clock) {
   // This Module has no inputs, so we specify a Signal bus of zero width for
   // the input parameter.
   Module((sd, "Clock"),
          Signal(0),
          (startRun, clock));

   Switch(SD(sd, "1a"), startRun, 'r');
   Pulser(SD(sd, "1a"), clock, 'c', 1000);
}
