/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef SIGN_EXT_H
#define SIGN_EXT_H

#include <Sim.h>

#define IN_SIZE 16
#define OUT_SIZE 32

void signExt(SD(sd), const Signals& in, const Signals& out);

#endif
