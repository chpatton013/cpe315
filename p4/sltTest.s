#   Lab Project #4 test file 2  (SLT tests)
#   CPE 315
#   Spring 2010
#
# Description:
#
#         Tests slt with overflows and sltu instructions 
#  
# Modifications:
# Notes:
#           Compile with -bare switch
#           Change the J instruction manually to accomodate your own addresses.
#
#  Data Area
.data
                                                                                
.text
                                                                                
main:
# Load a 1 for branch comparisons 
    addi $9, $0, 1

# Load some numbers to provide data for overflows and carrys 
    lui  $4, 0x9000 
    or   $4, $4, $0

    lui  $5, 0x5000
    or   $5, $5, $0 

# Test SLT with overflow 
    slt  $22, $4, $5
    beq  $22, $0, bad

# Test SLTU that generates no carry 
# Don't forget ALU generates carry incorrectly
    sltu $22, $4, $5
    beq  $22, $9, bad

# Test SLTU that generates carry 
    sltu $22, $5, $4
    beq  $22, $0, bad

# Stop here - done
done:
    j    done
    

# If you get here, something isn't working
bad: 
    lui  $2, 0xbad 
    addi $2,$2,0xbad
