/* romTest.c

   CPE 315 - Professor Grimes
   Spring 2012

   Written by:  Dan Stearns
   Date:        Fall 2002

   This simulation builds a 16 by 8 bit ROM with switches to
   drive the input.

   Modifications: none.

   Notes:
     The rom file must must in a precise format with 4 bytes
     per line.  This input format is independent of the ROM word size.
     For example, the romfile might look like this:

          0xA5 0x01 0x02 0x03
          0x04 0x05 0x06 0x07
          0x08 0x09 0x0a 0x0b
          0x0c 0x0d 0x0e 0x0f
*/

#include <loadRom.h>
#include <Sim.h>

// Define Rom geometry - 16 words; each word is 8 bits
#define ROMSIZE 16
#define WORDSIZE 8

// Number of words in romfile
#define NUM_WORDS 16

// Data structure for rom contents
unsigned char romContents[NUM_WORDS];

void simnet() {

   // input to ROM
   Signal address(4, "address");
   Switch("1a", address[3], '3');
   Switch("1a", address[2], '2');
   Switch("1a", address[1], '1');
   Switch("1a", address[0], '0');

   // output from ROM
   Signal bits(8, "ROM Output");

   // burn the rom
   loadRom(romContents, NUM_WORDS, "romfile.txt");

   // instantiate the rom
   Rom("1b", address, bits, ROMSIZE, WORDSIZE, romContents);

   Probe("1c", bits);
}

