/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "alu.h"

void alu(SD(sd),
   const Signals& A,
   const Signals& B,
   const Signal& Cin,
   const Signal& Binvert,
   const Signal& Unsgn,
   const Signal& LUI,
   const Signals& Op,

   const Signals& Res,
   const Signal& C,
   const Signal& V)
{
   Module((sd, "32 Bit ALU"),
      (A,B,Cin,Binvert,Unsgn,Op),
      (Res,C,V)
   );

   Signal Cout(NUM_BITS);
   Signal Temp(NUM_BITS);
   Signal MathTemp(NUM_BITS);

   // for LUI
   Signal LUItemp(NUM_BITS);

   // for SLT
   Signal lessIn;
   Signal lessOut;

   Signal notV;
   Signal notC;
   Signal notMSB;
   Signal notUnsgn;

   Signal lessJunk(NUM_BITS-1);
   Signal set;
   Signal setIntrm(3);

   Signal SLT;

   // for AND, ADD, OR
   Signal Prod1;
   Signal Prod2;

   oneBitALU(SD(sd, "1d"), A[0], B[0], Cin, lessIn, Binvert, Op,
    MathTemp[0], Cout[0], lessJunk[0]);

   for (int i = 1; i < NUM_BITS-1; ++i)
      oneBitALU(SD(sd,"1d"), A[i], B[i], Cout[i-1], Zero, Binvert, Op,
       MathTemp[i], Cout[i], lessJunk[i]);

   oneBitALU(SD(sd,"1d"), A[NUM_BITS-1], B[NUM_BITS-1], Cout[NUM_BITS-2], Zero,
    Binvert, Op, MathTemp[NUM_BITS-1], C, lessOut);

   // LUI
   for (int i = 0; i < NUM_BITS / 2; ++i)
      Or(SD(sd, "1d"), (Zero, Zero), LUItemp[i]);
   for (int i = NUM_BITS / 2; i < NUM_BITS; ++i)
      Or(SD(sd, "1d"), (B[i - NUM_BITS / 2], Zero), LUItemp[i]);
   // end LUI

   // SLT
   Not(SD(sd, "1d"), V, notV);
   Not(SD(sd, "1d"), C, notC);
   Not(SD(sd, "1d"), MathTemp[NUM_BITS-1], notMSB);
   Not(SD(sd, "1d"), Unsgn, notUnsgn);

   And(SD(sd, "1d"), (notUnsgn, notV, MathTemp[NUM_BITS-1]), setIntrm[0]);
   And(SD(sd, "1d"), (notUnsgn, V, notMSB), setIntrm[1]);
   And(SD(sd, "1d"), (Unsgn, notC), setIntrm[2]);

   Or(SD(sd, "1d"), setIntrm, set);
   And(SD(sd, "1d"), (set, lessOut), lessIn);
   // end SLT

   // ADD, AND, OR
   Iand(SD(sd, "1d"), (MathTemp[NUM_BITS-1]), (A[NUM_BITS-1],B[NUM_BITS-1]), Prod1);
   Iand(SD(sd, "1d"), (A[NUM_BITS-1], B[NUM_BITS-1]), (MathTemp[NUM_BITS-1]), Prod2);
   Or(SD(sd, "1d"), (Prod1, Prod2), V);
   // end ADD, AND, OR

   And(SD(sd, "1d"), (Op, set), SLT);
   Mux(SD(sd, "1d"), SLT, ((31 * Zero, set), MathTemp), Temp);
   Mux(SD(sd, "1d"), LUI, (LUItemp, Temp), Res);
}
