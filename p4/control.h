/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#ifndef CONTROL_H
#define CONTROL_H

#include <Sim.h>
#include <loadRom.h>

#ifdef NUM_BITS
#undef NUM_BITS
#endif
#define NUM_BITS 9

#ifdef ROMSIZE
#undef ROMSIZE
#endif
#define ROMSIZE 64

#ifdef WORDSIZE
#undef WORDSIZE
#endif
#define WORDSIZE 16

static unsigned char controlRom[ROMSIZE * WORDSIZE];

void control(SD(sd),
   const Signals& OpCode,
   const Signals& Out
);

#endif
