/*  fullAdd.c  

    CPE 315
    Full Adder Implementation
*/

#include <Sim.h>

void fullAdder(SD(sd),

               // Inputs:
               const Signal& A,
               const Signal& B,
               const Signal& Cin,

               // Outputs:
               const Signal& Sum,
               const Signal& Cout)
{ 
   // This is a Module constructor.
   // The first argument is the display name for the module.
   // The second argument lists all of the input Signals.
   // The third argument lists all of the output Signals.
   // If fullAdder() is called directly by simnet(), this will put a "black box"
   // named "Full Adder" with three input leads and two output leads in the
   // simulation window.
   Module((sd, "Full Adder"),
          (A, B, Cin),
          (Sum, Cout)
   );

   // Intermediate signals
   Signal AxorB;
   Signal AandB;
   Signal CandAxorB;

   // Because these components are inside a Module, they will not be visible in
   // the simulation.
   Xor(SD(sd, "1b"), (A, B), AxorB);
   And(SD(sd, "1b"), (A, B), AandB);
   And(SD(sd, "1b"), (AxorB, Cin), CandAxorB);
   Or (SD(sd, "1b"), (AandB, CandAxorB), Cout);
   Xor(SD(sd, "1b"), (Cin, AxorB), Sum);
}
