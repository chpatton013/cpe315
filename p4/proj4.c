/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include <Sim.h>

#include "clockUnit.h"
#include "fetch.h"
#include "alu.h"
#include "signExt.h"
#include "control.h"
#include "aluControl.h"

#define BUS_WIDTH 32

void simnet() {
   int parts[] = {4};

   Sig(clock, 1);
   Sig(startRun, 1);
   Signal PC(BUS_WIDTH, "PC");
   Signal NextPC(BUS_WIDTH, "NextPC");
   Signal Instr(BUS_WIDTH, "Instr");
   Sig(invClock, 1);

   Signal regDst;
   Signal jump;
   Signal branch;
   Signal MemToReg;
   Signal ALUOp(2);
   Signal memWrite;
   Signal ALUSrc;
   Signal regWrite;

   Signal regReadAddr(5);
   Signal regWriteAddr(5);
   Signal regReadData1(32);
   Signal regReadData2(32);
   Signal regReadData3(32, "Register");

   Signal ALUCtrl(6, "ALUCtrl");

   Signal immed(32);
   Signal aluMuxOut(32);
   Signal aluRes(32, "ALURes");
   Signal C;
   Signal V;

   Signal BranchTemp(3);
   Signal BranchALURes(32, "BranchALURes");
   Signal BranchC;
   Signal BranchV;
   Signal BranchMuxOut(32, "BranchMuxOut");
   Signal JumpMuxOut(32, "JumpMuxOut");

   Signal RAMReadData(32, "RamReadData");
   Signal ALURAMMux(32, "ALURAMMux");

   Probe("1a", PC, 1, parts);
   Probe("1b", Instr, 1, parts);

   Not("1c", clock, invClock);
   clockUnit("1c", startRun, clock);
   fetch("1c",
      startRun, One, clock, JumpMuxOut,
      NextPC, PC, Instr
   );

   control("1d", (Instr[31] - Instr[26]),
    (regDst, jump, branch, MemToReg, ALUOp, memWrite, ALUSrc, regWrite));
   Mux("1d", regDst, (Instr[15] - Instr[11], Instr[20] - Instr[16]), regWriteAddr);

   RegisterFile("1e",
    (regWrite, invClock, regWriteAddr,
    regWriteAddr, Instr[20] - Instr[16], Instr[25] - Instr[21]),
    ALURAMMux,
    (regReadData3, regReadData2, regReadData1),
    32, 32, 5, 3, 1);
   signExt("1e", (Instr[15] - Instr[0]), immed);

   aluControl("1f", (ALUOp, Instr[3] - Instr[0]), ALUCtrl);
   Mux("1f", ALUSrc, (immed, regReadData2), aluMuxOut);

   alu("1g", regReadData1, aluMuxOut,
    ALUCtrl[5], ALUCtrl[4], ALUCtrl[3], ALUCtrl[2], ALUCtrl[1] - ALUCtrl[0],
    aluRes, C, V);
   alu("1g", NextPC, (immed[29] - immed[0], Zero, Zero),
    Zero, Zero, Zero, Zero, (One, Zero),
    BranchALURes, BranchC, BranchV);

   And("1h", (BranchTemp[1], branch), BranchTemp[2]);
   Not("1h", BranchTemp[0], BranchTemp[1]);
   Or("1h", aluRes, BranchTemp[0]);
   Ram("1h", (memWrite, clock, 2 * (aluRes[8] - aluRes[0])),
    regReadData2, RAMReadData, 512, 32, 9, 1, 1);

   Mux("1i", MemToReg, (RAMReadData, aluRes), ALURAMMux);
   Mux("1i", BranchTemp[2], (BranchALURes, NextPC), BranchMuxOut);
   Mux("1i", jump, ((NextPC[31] - NextPC[28], Instr[25] - Instr[0], 2 * Zero),
    BranchMuxOut), JumpMuxOut);

   Probe("1j", regReadData3, 1, parts);
   Probe("1k", aluRes, 1, parts);
}
