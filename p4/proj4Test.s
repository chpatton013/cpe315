#   Lab Project #4 test file 1
#   CPE 315
#
# Written By:  Thee Sanpitakseree in the Summer 2006 quarter. 
#
# Modifications:
#
# Notes:
#   1.  Assemble this program with the -bare switch
#   2.  The J instruction needs to be changed manually to your
#       machine's correct address. 
#
#  Data Area
.data
                                                                                
.text
                                                                                
main:

# Test J and BEQ - loop runs twice
    addi $5, $0, 0x40
    addi $2, $0, 0x10
 loop:
    add  $2, $2, $2
    beq  $2, $5, endLoop
    j    loop

 endLoop:
    sw   $2, 480($0)
  
# Test sign-extension, ADDU and SUB
    addi $4, $0, 1
    addi $2, $0, -50
    addu $2, $2, $4
    sub  $3, $2, $4
    sw   $3, 483($4)
 
# Test SLT and SLTU instructions
# Branch to bad means SLT failed
    addi $4, $0, -5000
    addi $5, $0, 10
    addi $6, $0, 1
    slt  $3, $4, $5
    beq  $3, $0, bad
    slt  $3, $5, $4
    beq  $3, $6, bad
    sltu $3, $5, $4
    beq  $3, $0, bad
    sltu $3, $4, $5
    beq  $3, $6, bad
 
# Test AND, OR
    addi $2, $0, 10
    addi $3, $0, 15
    addi $4, $0, 27
    or   $2, $2, $3
    and  $2, $2, $4
    sw   $2, 512($0)
 
# Test LW and show result. Do screen prints after instruction runs
    addi $5, $0, 484
    lw   $2, -4($5)     # Screen print $2 = 0x00000040
    lw   $2, 0($5)      # Screen print $2 = 0xffffffce
    lw   $2, 28($5)     # Screen print $2 = 0x0000000b
    
# Test backwards BEQ and terminate
    lui  $2, 0xdeed 
    beq  $0, $0, main   # Screen print pc = 0x00000008 

# If you get here, something is wrong
bad: 
    lui  $2, 0xbad 
    addi $2,$2,0xbad
