/**
 * @author Glen Beebe
 * @author Christopher Patton
 */

#include "control.h"

void control(SD(sd),
   const Signals& OpCode,
   const Signals& Out)
{
   Signal bits(WORDSIZE, "ROM Output");

   loadRom(controlRom, ROMSIZE * WORDSIZE, "controlUnitRom.txt");
   Rom(SD(sd, "1a"), OpCode, bits, ROMSIZE, WORDSIZE, controlRom);

   for (int ndx = 0; ndx < NUM_BITS; ++ndx)
      Or(SD(sd, "1a"), (bits[ndx], Zero), Out[ndx]);
}
